package launcherForTournamentDirect;

import java.util.Date;

import common.Player;
import common.Team;
import common.Clock.ClockType;
import common.event.TournamentGroupWithDirectEliminatory;
import common.Clock;

public class MmiEmulatorAsMain {

	public MmiEmulatorAsMain() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TournamentGroupWithDirectEliminatory event = new TournamentGroupWithDirectEliminatory(null);

		Team homeTeam = new Team("ATTA Ablis premiere", 12781234);
		Team guestTeam = new Team("ATTA Ablis deuxieme", 12785678);
		homeTeam.addPlayer('A', new Player("Thibault", "T", new Date(Date.UTC(80,05,23,06,22,00)),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		homeTeam.addPlayer('B', new Player("Franck", "F", new Date(),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		homeTeam.addPlayer('C', new Player("Patrick", "P", new Date(),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		homeTeam.addPlayer('D', new Player("Laurent", "L", new Date(),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);

		homeTeam.addPlayerForBottomDouble('A');
		homeTeam.addPlayerForBottomDouble('C');
		homeTeam.addPlayerForTopDouble('D');
		homeTeam.addPlayerForTopDouble('B');

		guestTeam.addPlayer('X', new Player("Marius", "M",
				new Date(),
				Short.parseShort("1000"),
				Short.parseShort("1000"),
				Short.parseShort("1000"),
				780001,
				7812345)
				);
		guestTeam.addPlayer('Y', new Player("Jeremy", "J", new Date(),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		guestTeam.addPlayer('Z', new Player("Benoit", "B", new Date(),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		guestTeam.addPlayer('W', new Player("Laurent", "L", new Date(),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		
		guestTeam.addPlayerForBottomDouble('Z');
		guestTeam.addPlayerForBottomDouble('W');
		guestTeam.addPlayerForTopDouble('X');
		guestTeam.addPlayerForTopDouble('Y');

		event.generateMatches();
		event.flushMatchList();

		//Clock counter test
		Clock eventDuration = new Clock(ClockType.COUNTER);
		eventDuration.start();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Timeout occurs
		Clock timeout = new Clock(ClockType.TIMEOUT);
		timeout.start();

		try {
			Thread.sleep(65000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		eventDuration.endSession();
		try {
			eventDuration.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
