/**
 * 
 */
package common.event;

import java.util.Properties;

/**
 * @author maximillianeo
 * This class is used to manage a tournament divided
 * in groups depending on the number of players and after 
 * groups matches a table with deterministic rank starts.
 * Everybody plays the same number of games to determines
 * the right order of all players
 */ 
public class TournamentGroupWithFullRanking extends Event {

	/**
	 * @param name
	 * @param type
	 * @param properties
	 */
	public TournamentGroupWithFullRanking(String name, CompetType type,
			Properties properties) {
		super(name, type, properties);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see common.event.Event#generateMatches()
	 */
	@Override
	public boolean generateMatches() {
		// TODO Auto-generated method stub
		return false;
	}

}
