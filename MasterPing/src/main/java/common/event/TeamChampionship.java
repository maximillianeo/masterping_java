/**
 * 
 */
package common.event;

import java.util.ArrayList;
import java.util.Properties;

import common.HmiInterface;
import common.Match;
import common.Team;
import common.utils.Enums.ClassType;

/**
 * @author maximillianeo
 *
 */
public class TeamChampionship extends Event implements HmiInterface{

	private Team homeTeam = null;
	private Team guestTeam = null ;
	
	private final static String EventName = "Team championship";
	/**
	 * 
	 */
	public TeamChampionship(Properties properties) {
		super(TeamChampionship.EventName, CompetType.valueOf("TEAM_CHAMPIONSHIP"), properties);
		super.matches = new ArrayList<Match>(20);
		super.startEvent();
	}

	@Override
	public boolean generateMatches() {
		/*
		 * Match order from 2013's first phase
		 * A-W 1-1
		 * B-X 2-2
		 * C-Y 3-3
		 * D-Z 4-4
		 * A-X 1-2
		 * B-W 2-1
		 * D-Y 4-3
		 * C-Z 3-4
		 * DOUBLE1
		 * DOUBLE2
		 * A-Y 1-3
		 * C-W 3-1
		 * D-X 4-2
		 * B-Z 2-4
		 * ---------------------------------------------------------------
		 * ---------------------------------------------------------------
		 * Matches order until 2012
		 * A-X	1-1
		 * D-R	4-4
		 * B-Y	2-2
		 * E-S	5-5
		 * C-Z	3-3
		 * F-T	6-6
		 * B-X	2-1
		 * DOUBLE bottom part DEF-RST 456-456
		 * A-Z	1-3
		 * E-R	5-4
		 * C-Y	3-2
		 * D-T	4-6
		 * DOUBLE top part ABC-XYZ 123-123
		 * F-S	6-5
		 * B-Z	2-3
		 * E-T	5-6
		 * C-X	3-1
		 * F-R	6-4
		 * A-Y	1-2
		 * D-S	4-5
		 */
		if( null == homeTeam && null == guestTeam  ){
			return false;
		}
		matches.add(new Match(homeTeam.getPlayer('A'), guestTeam.getPlayer('W'),(short) (1),false));
		matches.add(new Match(homeTeam.getPlayer('B'), guestTeam.getPlayer('X'),(short) (1),false));
		matches.add(new Match(homeTeam.getPlayer('C'), guestTeam.getPlayer('Y'),(short) (1),false));
		matches.add(new Match(homeTeam.getPlayer('D'), guestTeam.getPlayer('Z'),(short) (1),false));
		matches.add(new Match(homeTeam.getPlayer('A'), guestTeam.getPlayer('X'),(short) (1),false));
		matches.add(new Match(homeTeam.getPlayer('B'), guestTeam.getPlayer('W'),(short) (1),false));
		matches.add(new Match(homeTeam.getPlayer('D'), guestTeam.getPlayer('Y'),(short) (1),false));
		matches.add(new Match(homeTeam.getPlayer('C'), guestTeam.getPlayer('Z'),(short) (1),false));
		matches.add(new Match(homeTeam.getPlayersForTopDouble(),guestTeam.getPlayersForTopDouble(), (short) (1),false));
		matches.add(new Match(homeTeam.getPlayersForBottomDouble(),guestTeam.getPlayersForBottomDouble(), (short) (1),false));		
		matches.add(new Match(homeTeam.getPlayer('A'), guestTeam.getPlayer('Y'),(short) (1),false));
		matches.add(new Match(homeTeam.getPlayer('C'), guestTeam.getPlayer('W'),(short) (1),false));
		matches.add(new Match(homeTeam.getPlayer('D'), guestTeam.getPlayer('X'),(short) (1),false));	
		matches.add(new Match(homeTeam.getPlayer('B'), guestTeam.getPlayer('Z'),(short) (1),false));

		return true;
	}
	
	/**
	 * @return the homeTeam
	 */
	public final Team getHomeTeam() {
		return homeTeam;
	}

	/**
	 * @param homeTeam the homeTeam to set
	 */
	public final void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	/**
	 * @return the guestTeam
	 */
	public final Team getGuestTeam() {
		return guestTeam;
	}

	/**
	 * @param guestTeam the guestTeam to set
	 */
	public final void setGuestTeam(Team guestTeam) {
		this.guestTeam = guestTeam;
	}
	
	public final String toString (){
		return getName();
		
	}
	
	public ClassType getObjectType(){
		return ClassType.EVENT;
	}
}
