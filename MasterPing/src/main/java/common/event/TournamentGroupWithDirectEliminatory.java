/**
 * 
 */
package common.event;

import java.util.Properties;

/**
 * @author maximillianeo
 * This class is used to manage a tournament divided
 * in groups depending on the number of players and after 
 * groups matches a direct eliminating end of table starts
 */
public class TournamentGroupWithDirectEliminatory extends  Event {

	/**
	 * 
	 */
	private final Short numberOfPlayersByGroup;
	
	/**
	 * 
	 */
	public TournamentGroupWithDirectEliminatory(Properties properties) {
		super(CompetType.TOURNAMENT_GROUP_WITH_ELIMINATION.toString(),
				CompetType.TOURNAMENT_GROUP_WITH_ELIMINATION,
				properties	);
		numberOfPlayersByGroup = Short.valueOf(getProperties().getProperty("playersByGroup"));
	}

	@Override
	public boolean generateMatches() {
		// TODO Auto-generated method stub
		return false;
		
	}

	/**
	 * @return the playersByGroup
	 */
	public final Short getNumberOfPlayersByGroup() {
		return numberOfPlayersByGroup;
	}

}
