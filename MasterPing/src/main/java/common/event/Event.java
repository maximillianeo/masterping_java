/**
 * 
 */
package common.event;

import java.util.List;
import java.util.Properties;
import java.util.Date;
import common.Match;

/**
 * @author maximillianeo
 *
 */
public abstract class Event{

	public enum CompetType{
		TOURNAMENT_GROUP_WITH_ELIMINATION,
		TOURNAMENT_ALL_VS_ALL,
		TEAM_CHAMPIONSHIP,
		PARIS_TEAM_CHAMPIONSHIP_EVENT
	}
		
	/**
	 * 
	 */
	private final String name;
	private final CompetType type;
	private final Properties properties;
	//TODO verify if vector is needed to be instantiated
	protected List <Match> matches;
	protected Date startingDate,
				   endingDate;	

	/**
	 * 
	 */
	public Event(String name, CompetType type,Properties properties) {
		this.properties = properties;
		this.name = properties.getProperty("event.name");
		this.type = type;
	}
	
	public abstract boolean generateMatches();
	
	public int getNumberOfMatches(){
		return matches.size();
	}
	
	protected boolean addMatchToCompetition (Match match){
		return matches.add(match);
	}
	
	/**
	 * @return the matches
	 */
	public final List<Match> getMatches() {
		return matches;
	}

	/**
	 * @param matches the matches to set
	 */
	public final void setMatches(List<Match> matches) {
		this.matches = matches;
	}

	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return the type
	 */
	public final CompetType getEventType() {
		return type;
	}

	/**
	 * @return the properties
	 */
	public final Properties getProperties() {
		return properties;
	}
	
	/**
	 * log to stdout the list of all match
	 */
	//TODO get log service and write data to that service
	public final void flushMatchList(){
		Short s = 1;
		for (Match match : matches){
			System.out.println("Match ("+s+") - " + match.toString());
			s++;
		}
	}
	
	protected void startEvent (){
		startingDate = new Date();
	}
	
	protected void endEvent(){
		endingDate = new Date();
	}
}
