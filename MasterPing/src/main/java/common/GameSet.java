/**
 * 
 */
package common;

import java.io.Serializable;
import common.Clock.TimeViewAsCouple;

/**
 * @author maximillianeo
 *
 */
public class GameSet implements Serializable{
	
	public enum ServiceSide {
		LEFT,
		RIGHT
	}
	private ServiceSide serviceSide;
	private long startTime;
	private TimeViewAsCouple currentTime;
	private Match match;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param serviceSide 
	 * 
	 */
	public GameSet(ServiceSide serviceSide, Match match) {
		this.serviceSide = serviceSide;
		this.startTime = System.currentTimeMillis();
		this.match = match;
	}
	
	public synchronized TimeViewAsCouple getCurrentTime() {
		long elapsed = System.currentTimeMillis() - startTime;
		currentTime.minutes = elapsed / 60000;
		currentTime.secondes = elapsed % 60000;
		return  currentTime;
	}

	/**
	 * @return the startTime
	 */
	protected final long getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	protected final void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the match
	 */
	protected final Match getMatch() {
		return match;
	}
	
	public void changeServiceSide (){
		this.serviceSide = this.serviceSide.equals(ServiceSide.LEFT)?
								ServiceSide.RIGHT: ServiceSide.LEFT ;
	}
	
	public ServiceSide getServiceSide(){
		return this.serviceSide;
	}
}
