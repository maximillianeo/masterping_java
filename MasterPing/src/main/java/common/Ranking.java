/**
 * 
 */
package common;

import java.io.Serializable;
import java.util.Date;

/**
 * @author maximillianeo
 *
 */
public class Ranking implements Serializable{

	private static Date Today = new Date(); 
	public enum RankingCategory {
		VETERAN5,
		VETERAN4,
		VETERAN3,
		VETERAN2,
		VETERAN1,
		SENIOR,
		JUNIOR,
		CADET,
		MINIME,
		BENJAMIN,
		NO_CATEGORY
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Short ranking;
	private final RankingCategory rankingCategory;
	private final Short startingYearPoints;
	private final Short startingPhasePoints;
	private final Short currentPoints;
	/**
	 * 
	 */
	public Ranking(Date birthday, Short startingPhasePoints, Short startingYearPoints, Short currentPoints) {
		this.ranking = generateRanking(currentPoints);
		this.rankingCategory = Ranking.EvaluateRankingCategoryFromDate(birthday);
		this.startingPhasePoints = startingPhasePoints;
		this.startingYearPoints = startingYearPoints;
		this.currentPoints = currentPoints;
	}
	
	private Short generateRanking(Short currentPoints) {
		return (short) (currentPoints/10);
	}
	
	public static RankingCategory EvaluateRankingCategoryFromDate(Date birthday){
		//TODO calculate ranking if not null
		//TODO write only one return
		RankingCategory rankCat = RankingCategory.NO_CATEGORY; 
		if(null != birthday){
			int years = Today.getYear() - birthday.getYear() ;
			if (years >= 80){
				rankCat = RankingCategory.VETERAN5;
			}else if (years >= 70){
				rankCat = RankingCategory.VETERAN4;
			}else if(years >= 60){
				rankCat = RankingCategory.VETERAN3;
			}else if (years >= 50){
				rankCat = RankingCategory.VETERAN2;
			}else if (years >= 40){
				rankCat = RankingCategory.VETERAN1;				
			}else if (years >= 20){
				rankCat = RankingCategory.SENIOR;
			}else if (years >= 16){
				rankCat = RankingCategory.JUNIOR;
			}else if (years >= 14){
				rankCat = RankingCategory.CADET;
			}else if (years >= 12){
				rankCat = RankingCategory.MINIME;
			}else if (years >= 10){
				rankCat = RankingCategory.BENJAMIN;
			}
		}
		return rankCat;
	}

	/**
	 * @return the serialversionuid
	 */
	public static final long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the ranking
	 */
	public final Short getRanking() {
		return ranking;
	}

	/**
	 * @return the rankingCategory
	 */
	public final RankingCategory getRankingCategory() {
		return rankingCategory;
	}

	/**
	 * @return the startingYearPoints
	 */
	public final Short getStartingYearPoints() {
		return startingYearPoints;
	}

	/**
	 * @return the startingPhasePoints
	 */
	public final Short getStartingPhasePoints() {
		return startingPhasePoints;
	}

	/**
	 * @return the currentPoints
	 */
	public final Short getCurrentPoints() {
		return currentPoints;
	}

}
