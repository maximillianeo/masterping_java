/**
 * 
 */
package common.utils;

/**
 * @author maximillianeo
 *
 */
public class Enums {

	public enum ActionTheme{
		HOME_PLAYER_SCORE,
		HOME_PLAYER_SET,
		HOME_PLAYER_TIMEOUT,
		GUEST_PLAYER_SCORE,
		GUEST_PLAYER_SET,
		GUEST_PLAYER_TIMEOUT,
		DEATH_MATCH_ACTIVE,
		NO_THEME
	}
	
	public enum ClassType{
		MATCH,
		PLAYER,
		TEAM,
		EVENT
	}
	
	public enum TeamSide{
		HOME,
		GUEST
	}

}
