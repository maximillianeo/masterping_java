/**
 * 
 */
package common.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author maximillianeo
 *
 */
//TODO add 2 generic parameters
public class BidirectionnalHashMap <K,V> {

	/**
	 * 
	 */
	private final Map<K, V> t1t2map;
	private final Map<V, K> t2t1map;

	public BidirectionnalHashMap(Integer size) {
		t1t2map = new HashMap<K, V> (size);
		t2t1map = new HashMap<V, K> (size);
	}
	public void put(K index, V value){
		t1t2map.put(index, value);
		t2t1map.put(value, index);
	}

	public V getByKey(K index){
		return t1t2map.get(index);
	}

	public K getByValue(V value){
		return t2t1map.get(value);
	}
	
	public Integer size(){
		return t1t2map.size();
	}
	
	public Collection<V> values(){
		return t1t2map.values();
	}

}
