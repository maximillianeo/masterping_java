/**
 * 
 */
package common;

/**
 * @author maximillianeo
 *
 */
public class Clock extends Thread {

	public enum ClockType {COUNTER,
				TIMEOUT
	}
	public class TimeViewAsCouple{
		public long minutes;
		public long secondes;
		public TimeViewAsCouple() {
			this.minutes = 0;
			this.secondes = 0;
		}
	}
	
	/**
	 * 
	 */
	private boolean endSession, timeout;
	private TimeViewAsCouple currentClock;
	private ClockType type;
	
	public Clock(ClockType type) {
		this.endSession = false;
		this.currentClock = new TimeViewAsCouple();
		this.type = type;
		this.timeout = type.equals(ClockType.COUNTER)?false:true;
		this.currentClock.secondes = timeout?60:0;
	}

	/**
	 * @param target
	 */
	public Clock(Runnable target) {
		super(target);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param name
	 */
	public Clock(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param group
	 * @param target
	 */
	public Clock(ThreadGroup group, Runnable target) {
		super(group, target);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param group
	 * @param name
	 */
	public Clock(ThreadGroup group, String name) {
		super(group, name);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param target
	 * @param name
	 */
	public Clock(Runnable target, String name) {
		super(target, name);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param group
	 * @param target
	 * @param name
	 */
	public Clock(ThreadGroup group, Runnable target, String name) {
		super(group, target, name);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param group
	 * @param target
	 * @param name
	 * @param stackSize
	 */
	public Clock(ThreadGroup group, Runnable target, String name, long stackSize) {
		super(group, target, name, stackSize);
		// TODO Auto-generated constructor stub
	}
	
	public void run(){
		while (!endSession){				
			try {
				//long laps = timeout?60000:1000;
				//Thread.sleep(laps);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(timeout){
				currentClock.secondes --;
			}else{
				currentClock.secondes++;
			}
			
			if (60 == currentClock.secondes){
				currentClock.secondes = 0;
				currentClock.minutes++;
			}else if (0 == currentClock.secondes){
				endSession();
				//TODO generate a sound to signal the end of timeout
			}
			
			//TODO to remove when logging enable
			//TODO prints with 2 digit min and sec
			System.out.println(type.toString() + "--" + currentClock.minutes + "m:" + currentClock.secondes + "s");
		}
	}

	/**
	 * @return the endMatch
	 */
	public final boolean isEndSession() {
		return endSession;
	}

	/**
	 * end the Match setting endsession to true
	 */
	public final void endSession() {
		 endSession = true;
	}
	/**
	 * @param endSession the endMatch to set
	 */
	public final void setEndSession(boolean endSession) {
		this.endSession = endSession;
	}

	/**
	 * @return the timeout
	 */
	public final boolean isTimeout() {
		return timeout;
	}

	/**
	 * @param timeout the timeout to set
	 */
	public final void endTimeout() {
		this.timeout = false;
	}

	/**
	 * @return the currentClock
	 */
	public synchronized final TimeViewAsCouple getCurrentClock() {
		return currentClock;
	}

	/**
	 * @param currentClock the currentClock to set
	 */
	public synchronized final void setCurrentClock(TimeViewAsCouple currentClock) {
		this.currentClock = currentClock;
	}
	

}
