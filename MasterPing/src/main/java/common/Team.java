/**
 * 
 */
package common;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import common.utils.BidirectionnalHashMap;
import common.utils.Enums.ClassType;
import common.utils.Enums.TeamSide;

/**
 * @author maximillianeo
 *
 */
public class Team implements HmiInterface{

	/**
	 * 
	 */
	
	private final String name;
	private Integer teamNumber;
	
	private Vector<Player> 	playersForTopDouble,
							playersForBottomDouble;
	
	private BidirectionnalHashMap<Character, Player> players;

	
	public Team(String name, Integer teamNumber) {
		this.name = name;
		this.teamNumber = teamNumber;

		
		players = new BidirectionnalHashMap<Character, Player>(4);
		playersForBottomDouble = new Vector<Player>(2);
		playersForTopDouble = new Vector<Player>(2);
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public int addPlayer (Character index, Player player){
		//TODO manage letter case here or in generateMatches method
		players.put(index, player);
		return players.size();		
	}
	
	public Player getPlayer(Character index){
		return players.getByKey(index);
	}
	
	public Boolean addPlayerForTopDouble(Character index){
		return playersForTopDouble.add(players.getByKey(index)); 
	}
	
	public int clearPlayersForTopDouble(){
		playersForTopDouble.clear();
		return playersForTopDouble.size();
	}
	
	public Boolean addPlayerForBottomDouble(Character index){
		return playersForBottomDouble.add(players.getByKey(index)); 
	}
	
	public int clearPlayersForBottomDouble(){
		playersForBottomDouble.clear();
		return playersForBottomDouble.size();
	}
	
	public Vector<Player> getPlayersForTopDouble(){
		return playersForTopDouble;
	}
	
	public Vector<Player> getPlayersForBottomDouble(){
		return playersForBottomDouble;
	}
	public Character getPlayerLetter(Player p){
		return players.getByValue(p);
	}

	@Override
	public ClassType getObjectType() {
		return ClassType.TEAM;
	}
	
	public String toString(){
		return name + "-" + teamNumber;
	}

	public Collection<Player> getPlayers() {
		return players.values();
	}
}
