/**
 * 
 */
package common;

import java.util.List;
import java.util.Vector;

import common.GameSet.ServiceSide;
import common.utils.Enums.ClassType;

/**
 * @author maximillianeo
 *
 */
public class Match implements HmiInterface{

	private final Player homePlayer;
	private final Player guestPlayer;
	private final Vector<Player> guestPlayers;
	private final Vector<Player> homePlayers;
	private Short tableNumber;
	private List<GameSet> sets;
	private GameSet currentSet;
	private Clock matchTime;
	private final Boolean instantDeath;
	private final Boolean isDouble;
	
	/**
	 * @param homeplayerName
	 * @param guestplayerName
	 * @param instantDeath
	 */
	public Match(Player homeplayer, Player guestplayer,
			Short tableNumber, Boolean instantDeath) {
		this.homePlayer = homeplayer;
		this.guestPlayer = guestplayer;
		this.homePlayers = null;
		this.guestPlayers = null;
		this.instantDeath = instantDeath;
		this.tableNumber = 0;
		this.isDouble = false;
	}
	public Match(Vector<Player> homePlayers, Vector<Player> guestPlayers,
			Short tableNumber, Boolean instantDeath) {
		this.homePlayers = homePlayers;
		this.guestPlayers = guestPlayers;
		this.homePlayer = null;
		this.guestPlayer =null;
		this.instantDeath = instantDeath;
		this.tableNumber = 0;
		this.isDouble = true;
	}
	/**
	 * 
	 */
	public GameSet start(ServiceSide serviceSide){
		this.sets = new Vector<GameSet>();
		this.matchTime.start();
		return beginSet(serviceSide);
	}
	 
	 private GameSet beginSet(ServiceSide serviceSide) {
		return new GameSet(serviceSide, this);		
	}
	/**
	 * @return the homeplayerName
	 */
	public Player getHomePlayerName() {
		return homePlayer;
	}
	/**
	 * @return the guestplayerName
	 */
	public String getGuestPlayerName() {
		return guestPlayer.getName();
	}
	/**
	 * @return the homeplayerNamesForDouble
	 */
	public  Vector<Player> getHomePlayersForDouble() {
		return homePlayers;
	}
	/**
	 * @return the guestplayerNameForDouble
	 */
	public Vector<Player> getGuestPlayersForDouble() {
		return guestPlayers;
	}
	/**
	 * @return the tableNumber
	 */
	public Short getTableNumber() {
		return tableNumber;
	}
	/**
	 * @return the sets
	 */
	public List<GameSet> getSets() {
		return sets;
	}
	/**
	 * @return the currentSet
	 */
	public GameSet getCurrentSet() {
		return currentSet;
	}
	/**
	 * @return the matchTime
	 */
	public Clock getMatchTime() {
		return matchTime;
	}
	/**
	 * @return the instantDeath
	 */
	public Boolean getInstantDeath() {
		return instantDeath;
	}
	/**
	 * @param sets the sets to add
	 */
	public void setSets(List<GameSet> sets) {
		this.sets = sets;
	}
	/**
	 * @param set the set to add to the list
	 */
	public void addSet(GameSet set) {
		this.sets.add(set);
	}
	/**
	 * @param currentSet the currentSet to set
	 */
	public void setCurrentSet(GameSet currentSet) {
		this.currentSet = currentSet;
	}
	/**
	 * @param matchTime the matchTime to set
	 */
	public void setMatchTime(Clock matchTime) {
		this.matchTime = matchTime;
	}
	@Override
	public ClassType getObjectType() {
		return ClassType.MATCH;
	}
	
	public String toString(){
		String returnValue;
		if(isDouble){
			returnValue = homePlayers.get(0).getLastName() + "/" + homePlayers.get(1).getLastName()
						+ " -VS- " +
						guestPlayers.get(0).getLastName() + "/" + guestPlayers.get(1).getLastName();
		}else{
			returnValue = getHomePlayerName() + " -VS- " + getGuestPlayerName();
		}
		
		return returnValue;
	}
	
	public Boolean isDouble(){
		return this.isDouble;
	}
}
