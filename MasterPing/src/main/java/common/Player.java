/**
 * 
 */
package common;

import java.util.Date;

import common.utils.Enums.ClassType;

/**
 * @author maximillianeo
 *
 */
public class Player implements HmiInterface{

	/**
	 * 
	 */
	private final String firstName;
	private final String lastName;
	private final Date birthday;
	private final Integer licenseNbr;
	private final Integer clubNbr;
	private final Ranking ranking;
	
	/**
	 * 
	 */
	public Player(String firstName, String lastName, Date birthday, 
			Short startingYearPoints, Short startingPhasePoints,
			Short currentPoints,
			Integer licenseNbr, Integer clubNbr) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		
		this.ranking = new Ranking (birthday, startingPhasePoints, startingYearPoints, currentPoints);
		
		this.licenseNbr = licenseNbr;
		this.clubNbr = clubNbr;		
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * @return the licenseNbr
	 */
	public Integer getLicenseNbr() {
		return licenseNbr;
	}

	/**
	 * @return the clubNbr
	 */
	public Integer getClubNbr() {
		return clubNbr;
	}

	/**
	 * @return the ranking
	 */
	public Ranking getRanking() {
		return ranking;
	}

	@Override
	public ClassType getObjectType() {
		return ClassType.PLAYER;
	}

	public String getName() {
		return firstName;
	}
	
	public String toString(){
		return firstName + " " + lastName ;
	}

}
