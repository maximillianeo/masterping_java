/**
 * 
 */
package common;

/**
 * @author maximillianeo
 *
 */
public class Oponent {

	/**
	 * 
	 */
	private Short currentSetScore = 0;
	private Boolean isServer = false;
	private Boolean isTimeOutUsed = false;
	private Short nbOfWinnedSet = 0;
	/**
	 * 
	 */
	public Oponent(String playerName) {
		// TODO Auto-generated constructor stub
	}
	
	public void winnedPoint(){
		this.currentSetScore ++ ;
	}
	
	public Short getCurrentSetScore(){
		return this.currentSetScore;
	}
	
	public void useTimeOut(){
		this.isTimeOutUsed = true;
	}
	public Boolean isTimeOutAvailable(){
		return this.isTimeOutUsed;
	}
	
	public void winnedSet(){
		this.nbOfWinnedSet ++;
	}
	
	public void giveService(){
		this.isServer = true;
	}
	
	public Boolean hasService(){
		return this.isServer;
	}
	
}
