package mmi.server;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Date;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;

import mmi.server.menu.CreateEventTeamChampionshipMenu;
import mmi.server.menu.QuitMenu;
import common.Match;
import common.Player;
import common.Team;
import common.event.Event;
import common.event.TeamChampionship;
import common.utils.Enums.TeamSide;

public class FirstScreen extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTree jt_eventList;
	private JLabel selectedLabel;
	private Properties properties = new Properties();
	JTabbedPane panelOnglet;

	public FirstScreen(){
		buildMenu();
		setProperties();
		getProperties();			
		setDecoration();
		createJtree();
		createStaticTabPanel();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		createScoreHeadBand();
	}

	private void buildMenu() {

		JMenuBar menuBar = new JMenuBar();

		JMenu fileMenu = new JMenu("File");
		JMenu eventMenu = new JMenu("Event");		
		
		JMenu eventSelection = new JMenu("New");
		eventMenu.add(eventSelection);

		JMenuItem newEventTeamChampionship = new JMenuItem(new CreateEventTeamChampionshipMenu(this, "TeamChampionship"));
		eventSelection.add(newEventTeamChampionship);

		JMenuItem exitMenu = new JMenuItem(new QuitMenu("Quitter"));
		fileMenu.add(exitMenu);

		menuBar.add(fileMenu);
		menuBar.add(eventMenu);

		setJMenuBar(menuBar);

	}

	private void getProperties() {
		//TODO verify method is useful
		String event = properties.getProperty("event.name");
		String teamHome = properties.getProperty("team.home.name");
		String teamExt  = properties.getProperty("team.ext.name");
		String playerHomeA = properties.getProperty("player.home.A");
		String playerHomeB = properties.getProperty("player.home.B");
		String playerHomeC = properties.getProperty("player.home.C");
		String playerHomeD = properties.getProperty("player.home.D");
		String playerExtA = properties.getProperty("player.ext.A");
		String playerExtB = properties.getProperty("player.ext.B");
		String playerExtC = properties.getProperty("player.ext.C");
		String playerExtD = properties.getProperty("player.ext.D");

	}

	private void setProperties() {
		properties.setProperty("event.name", "Championnat par �quipe Journ�e n�6 - Br�lage n�8");
		properties.setProperty("team.home.name", "Ablis 1");
		properties.setProperty("team.ext.name", "Sartouville 3");
		properties.setProperty("team.home.id", "12781234");
		properties.setProperty("team.ext.id", "12789876");

		String playerHomeA = properties.getProperty("player.home.A");
		String playerHomeB = properties.getProperty("player.home.B");
		String playerHomeC = properties.getProperty("player.home.C");
		String playerHomeD = properties.getProperty("player.home.D");
		String playerExtA = properties.getProperty("player.ext.A");
		String playerExtB = properties.getProperty("player.ext.B");
		String playerExtC = properties.getProperty("player.ext.C");
		String playerExtD = properties.getProperty("player.ext.D");

	}

	private void createJtree(){
		//TODO uses property to initialize event
		TeamChampionship event = new TeamChampionship(properties);		
		//create the root node
		DefaultMutableTreeNode jt_root = new DefaultMutableTreeNode(event);

		//create the child nodes		
		Team homeTeam = new Team(properties.getProperty("team.home.name"), Integer.parseInt(properties.getProperty("team.home.id")));
		DefaultMutableTreeNode jt_homeTeam = new DefaultMutableTreeNode(homeTeam);

		Team extTeam = new Team(properties.getProperty("team.ext.name"), Integer.parseInt(properties.getProperty("team.ext.id")));
		DefaultMutableTreeNode jt_extTeam = new DefaultMutableTreeNode(extTeam);

		DefaultMutableTreeNode jt_champParEquipe = new DefaultMutableTreeNode("Division 3: " + homeTeam.getName() + " -VS- " + extTeam.getName());
		//TEAMs
		//TODO create a panel to set each player's team
		homeTeam.addPlayer('A', new Player("Thibault", "T", new Date(Date.UTC(80,05,23,06,22,00)),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		homeTeam.addPlayer('B', new Player("Franck", "F", new Date(Date.UTC(80,01,10,06,22,00)),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		homeTeam.addPlayer('C', new Player("Patrick", "P", new Date(Date.UTC(80,02,12,06,22,00)),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		homeTeam.addPlayer('D', new Player("Laurent", "L", new Date(Date.UTC(80,03,13,06,22,00)),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);

		homeTeam.addPlayerForBottomDouble('A');
		homeTeam.addPlayerForBottomDouble('C');
		homeTeam.addPlayerForTopDouble('D');
		homeTeam.addPlayerForTopDouble('B');

		extTeam.addPlayer('X', new Player("Marius", "M",new Date(Date.UTC(80,02,29,06,22,00)),
				Short.parseShort("1000"),Short.parseShort("1000"),Short.parseShort("1000"),
				780001,7812345)
				);
		extTeam.addPlayer('Y', new Player("Jeremy", "J", new Date(Date.UTC(80,07,27,06,22,00)),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		extTeam.addPlayer('Z', new Player("Benoit", "B", new Date(Date.UTC(80,04,28,06,22,00)),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);
		extTeam.addPlayer('W', new Player("Laurent", "L", new Date(Date.UTC(80,10,30,06,22,00)),
				Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
				780001, 7812345)
				);

		extTeam.addPlayerForBottomDouble('Z');
		extTeam.addPlayerForBottomDouble('W');
		extTeam.addPlayerForTopDouble('X');
		extTeam.addPlayerForTopDouble('Y');

		event.setHomeTeam(homeTeam);
		event.setGuestTeam(extTeam);

		event.generateMatches();
		//TEAMs      

		for(Player p : homeTeam.getPlayers()){
			jt_homeTeam.add(new DefaultMutableTreeNode(p));
		}
		for(Player p : extTeam.getPlayers()){
			jt_extTeam.add(new DefaultMutableTreeNode(p));
		}

		jt_champParEquipe.add(jt_homeTeam);
		jt_champParEquipe.add(jt_extTeam);

		DefaultMutableTreeNode matchList = new DefaultMutableTreeNode("Match list");

		for(Match m : event.getMatches()){
			matchList.add(new DefaultMutableTreeNode(m));
		}

		jt_champParEquipe.add(matchList);
		//add the child nodes to the root node
		jt_root.add(jt_champParEquipe);

		//create the tree by passing in the root node
		jt_eventList = new JTree(jt_root);
		//ImageIcon imageIcon = new ImageIcon(MasterPing.class.getResource("/endCall.jpg"));
		//ImageIcon defaultIcon = new ImageIcon(MasterPing.class.getResource("/bebeMario.png"));
		DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();       
		//renderer.setLeafIcon(imageIcon);
		//renderer.setIcon(defaultIcon);

		jt_eventList.setCellRenderer(renderer);
		jt_eventList.setEditable(true);
		jt_eventList.setShowsRootHandles(true);
		jt_eventList.setRootVisible(true);
		this.add(new JScrollPane(jt_eventList), BorderLayout.WEST);

		selectedLabel = new JLabel();
		this.add(selectedLabel, BorderLayout.SOUTH);

		jt_eventList.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jt_eventList.getLastSelectedPathComponent();
				if(null != selectedNode){
					selectedLabel.setText(selectedNode.getUserObject().toString()+"###");
				}
			}
		});
		MouseListener ml = new MouseAdapter() {
			public void mousePressed(MouseEvent e) {				
				int selRow = jt_eventList.getRowForLocation(e.getX(), e.getY());
				TreePath selPath = jt_eventList.getPathForLocation(e.getX(), e.getY());
				if(selRow != -1) {
					// if(e.getClickCount() == 1) {
					// 		System.out.println("One clicks pressed <" + selRow + "><"+selPath+">");
					//}
					if(e.getClickCount() == 2) {
						System.out.println("Two clicks pressed <" + selRow + "><"+selPath+">");
						DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode)jt_eventList.getLastSelectedPathComponent();
						Object currentObject = currentNode.getUserObject();
						//TODO replace by isinstanceof
						if (Event.class.isInstance(currentObject) ){
							//TODO Open jpane of selected player with its information
							Event currentEvent = (Event)currentObject;
							System.out.println("Double click on the event named --" + currentEvent.getName() + " of type --" + currentEvent.getEventType());
						}else if(Team.class.isInstance(currentObject) ){
							//TODO Open jpane of selected team with list of players and information
							Team currentTeam = (Team)currentObject;
							System.out.println("Double click on a TEAM named --" + currentTeam.getName());
						}else if(Player.class.isInstance(currentObject) ){
							//TODO Open jpane of selected player with its information
							Player currentPlayer = (Player)currentObject;
							System.out.println("Double click on a Player named --" + currentPlayer.getName());
						}else if(Match.class.isInstance(currentObject) ){
							//TODO Open jpane of selected match
							Match currentMatch = (Match) currentObject;
							createTabForMatchAndAddInPanel(currentMatch);
							System.out.println("Double click on a match, new panel opened");
						}else{
							System.out.println("Entry not managed or no userObject set in Jtree");
						}
					}
				}
			}
		};
		jt_eventList.addMouseListener(ml); 

	}

	private void createTabForMatchAndAddInPanel(Match match){
		JPanel jp = new JPanel();
		JLabel jl = new JLabel("Object with the match");
		
		jp.add(jl);

		this.panelOnglet.addTab(match.toString(), null, jp);
		this.getContentPane().repaint();
	}

	private void createStaticTabPanel(){
		JPanel jp1 = new JPanel();
		JPanel jp2 = new JPanel();
		JLabel jl1 = new JLabel("Le label 1");
		JLabel jl2 = new JLabel("Le label 2");
		
		this.panelOnglet = new JTabbedPane();
		jp1.add(jl1);
		jp2.add(jl2);

		panelOnglet.addTab("Panel1", null, jp1);
		panelOnglet.addTab("Panel2", null, jp2);

		this.getContentPane().add(panelOnglet);
	}

	private void createScoreHeadBand(){
		JPanel eventHeadBandScore = new JPanel();

	}
	private void setDecoration(){
		setTitle("MasterPing 2014 - 2018 -- ABLIS TT");
		setBackground(Color.GRAY);
		setSize(500, 300);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		FirstScreen mainFrame = new FirstScreen();

		mainFrame.setVisible(true);
	}

}
