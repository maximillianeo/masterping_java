package mmi.server.menu;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

public class QuitMenu extends AbstractAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public QuitMenu(String texte){
		super(texte);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.exit(0);
	}

}
