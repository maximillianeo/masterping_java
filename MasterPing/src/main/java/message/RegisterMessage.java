/**
 * 
 */
package message;

import java.io.Serializable;

/**
 * @author maximillianeo
 *
 */
public class RegisterMessage extends AMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final static Short messageId = 1;
	private final static Byte messageVersion = Byte.parseByte("0001");
	
	/**
	 * @param message
	 * @param tableNumber
	 */
	public RegisterMessage(Short tableNumber) {
		super(RegisterMessage.messageId, tableNumber);
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the messageid
	 */
	public static Short getMessageid() {
		return messageId;
	}

	/**
	 * @return the messageversion
	 */
	public static Byte getMessageversion() {
		return messageVersion;
	}

}
