/**
 * 
 */
package message;

import java.io.Serializable;

/**
 * @author maximillianeo
 *
 */
public class StartMatch extends AMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final static Short messageId = 3;
	
	/**
	 * @param messageId
	 * @param serialversionuid
	 */
	public StartMatch(Short messageId, Short tableNumber) {
		super(messageId, tableNumber);
		// TODO Auto-generated constructor stub
	}

}
