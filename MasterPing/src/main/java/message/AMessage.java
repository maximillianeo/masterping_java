/**
 * 
 */
package message;

import java.io.Serializable;

/**
 * @author maximillianeo
 *
 */
public abstract class AMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final Byte protocolVersion = Byte.parseByte("0001");
	protected Short messageId;
	protected Short tableNumber = -1;
	
	/**
	 * 
	 */
	protected AMessage(Short messageId, Short tableNumber) {
		this.messageId = messageId;
		this.tableNumber = tableNumber;
	}

}
