/**
 * 
 */
package message;

import java.io.Serializable;

import common.utils.Enums.ActionTheme;

/**
 * @author maximillianeo
 *
 */
public class Action extends AMessage implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final static Short messageId = 5;
	private final ActionTheme messageTheme;
	private final String actionValue;
	
	/**
	 * @param messageId
	 * @param tableNumber
	 */
	public Action(Short messageId, Short tableNumber, ActionTheme messageTheme, String actionValue) {
		super(messageId, tableNumber);
		this.messageTheme = messageTheme;
		this.actionValue = actionValue;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the messageid
	 */
	public static Short getMessageid() {
		return messageId;
	}

	/**
	 * @return the messageTheme
	 */
	public ActionTheme getMessageTheme() {
		return messageTheme;
	}

	/**
	 * @return the actionValue
	 */
	public String getActionValue() {
		return actionValue;
	}

}
