/**
 * 
 */
package message;

import java.io.Serializable;

import common.Match;

/**
 * @author maximillianeo
 *
 */
public class AcknowledgeMessage extends AMessage implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final static Short messageId = 2;
	private Match currentMatch;
	
	/**
	 * @param messageId
	 * @param messageVersion
	 */
	public AcknowledgeMessage(Short tableNumber, Match matchToManage) {
		super(messageId, tableNumber);
		this.currentMatch = matchToManage;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the messageid
	 */
	public static Short getMessageid() {
		return messageId;
	}

	/**
	 * @return the currentMatch
	 */
	public Match getCurrentMatch() {
		return currentMatch;
	}

}
