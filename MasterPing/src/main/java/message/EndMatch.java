/**
 * 
 */
package message;

import java.io.Serializable;

/**
 * @author maximillianeo
 *
 */
public class EndMatch extends AMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected final static Short messageId = 4;
	
	/**
	 * @param messageId
	 * @param tableNumber
	 */
	public EndMatch(Short messageId, Short tableNumber) {
		super(messageId, tableNumber);
		// TODO Auto-generated constructor stub
	}

}
